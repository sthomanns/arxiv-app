from django.contrib import admin
from django.urls import path
from app import views

urlpatterns = [
    path('', views.TopicsList),
    path('authors/', views.AuthorsList),
    path('works/', views.WorksList),
    path('publication/', views.Publication)
]