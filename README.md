# arXiv Django App

A Django app that ingests data from the arXiv bulk API and presents new articles to the user. 

## Setup

First you may need to install Redis and Conda package manager (or see below for Docker usage) if they are not already installed on your system, 

        brew install redis
        brew cask install conda 

Then generate and start the conda environment, 

        conda env create --file environment.yml
        conda activate arxiv-app
        
Finally, start the default Redis server, run the django test cases and start the server instance, 

        redis-server --daemonize yes
        python3 manage.py test
        python3 manage.py runserver   

## Docker

Use docker-compose.yml and the Dockerfile to run the Django server and Redis inside of a container, 

        docker-compose up --build

Navigate to http://127.0.0.1:8000/ to view the topic list, and http://127.0.0.1:8000/authors to view the list of the most prolific authors.