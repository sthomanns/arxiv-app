FROM python:3.7-alpine
RUN mkdir -p /opt/app
RUN mkdir -p /opt/app/arxiv
COPY requirements.txt /opt/app/
COPY . /opt/app/arxiv/
WORKDIR /opt/app
RUN pip install -r requirements.txt