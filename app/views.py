import sys
from urllib.parse import quote, urlparse
from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic.list import ListView 
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.http import JsonResponse
from django.core import serializers
from django.conf import settings
from .models import * 
import json


@api_view(["GET"])
def TopicsList(req):
    ''' Lists the newest publications across a few different categories
    '''
    topics = lazyload_topics()()
    return render(req, 'topics.html', { 'topics': topics })

@api_view(["GET"])
def AuthorsList(req):
    ''' Lists the authors in descending order of how many publications they've  
        each produced in the last 6 months
    '''
    authors, counts = lazyload_authors()()
    return render(req, 'authors.html', { 'counts': counts })

@api_view(["GET"])
def WorksList(req):
    ''' Lists all the publications an author is listed on that came out within 
        the last 6 months 
    '''
    authors, counts = lazyload_authors()()
    key = req.GET.get('author', '')
    key_enc = quote(key)
    return render(req, 'works.html', { 'works': authors[key_enc], 'author': key_enc, 'author_name': key })

@api_view(["GET"])
def Publication(req):
    ''' Lists details about a specific publication, such as summary and all of 
        its authors 
    '''
    authors, counts = lazyload_authors()()
    url_id = req.GET.get('publication_url', '')
    author = req.GET.get('author', '')
    author_enc = quote(author)
    publ = fetch_publication(author_enc, url_id)
    return render(req, 'publication.html', { 'publication': publ })
