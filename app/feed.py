import feedparser as fp 
import time
import urllib
from urllib.parse import urlparse, urlencode, quote
from datetime import datetime, timedelta
from itertools import chain
from typing import Generator

TOPICS = [  'cat:stat.ML',              # machine learning
                'all:data%20science',       # data science
                'all:therapy',              # therapy 
                'all:psychiatry' ]          # psychiatry

def __templ__(query: str, batch_count: int): 
  ''' Formats the root URL for arXiv bulk data API queries with parameters
  '''
  return f'http://export.arxiv.org/api/query?search_query=' \
                    f'{query}' \
                    f'&sortBy=submittedDate' \
                    f'&sortOrder=descending' \
                    f'&max_results={batch_count}'


def fill_feed(query='', batch_step=0, batches=None): 
  ''' Get atom feed results from arxiv up to a max amount of returned records, currently 
      hardcoded to 182 days (6 months) so thats the most stored in the Redis cache at any 
      given time. 
      Args
      ----
        query (str): the search condition for arXiv 
        batch_step (int): the current step in the list of batches
        batches (list<int>): a list of batch sizes to step through recursively
                             until at least 6 months of data has been consumed
                             (if available)
  ''' 
  get_time = lambda dt: datetime.strptime(dt, "%Y-%m-%dT%H:%M:%SZ")
  get_delta = lambda: datetime.now() - timedelta(days=182)

  batch_count = batches[batch_step]
  delta = get_delta()
  feed = fp.parse(__templ__(query, batch_count))                              # query the atom feed up to maximum # results
  matches = [k for k in feed.entries if get_time(k.published) > delta]     # get all published within last 6 months

  if (batch_step < len(batches) - 1 and batch_count == len(matches)):         # query more results if full 6 months not retrieved
    batch_step += 1
    return fill_feed(query=query, batch_step=batch_step, batches=batches)

  return (matches,                              
            feed.feed.opensearch_totalresults,  
            feed.feed.updated)

def fill_topics(*updated_times: list):
  ''' This function is designed to be called every 24 hours, since the arXiv api documentation 
      states that 24 hours is the frequency of their content updates and we would want to save 
      bandwidth on the server. If 24 hours have not elapsed since the last updates for a topic,
      that topic is skipped. Generator of the 5 newest publications across each topic. 
      Args
      ----
        updated_times (list): a list of timestamps indicating when each topic 
                                    was updated last 
  '''
  topic_feeds = dict(zip(TOPICS, updated_times))

  for topic, last_updated in topic_feeds.items():
    utcnow = datetime.utcnow()
    if (last_updated is not None and last_updated > (utcnow - timedelta(days=1))):
      continue

    topic_feed, topic_count, topic_upd = fill_feed(query=topic, batch_step=0, batches=[5])
    if (last_updated is None):
      topic_feeds[topic] = datetime.strptime(topic_upd, '%Y-%m-%dT%H:%M:%S-04:00')
      yield (topic_feed, topic_upd)

def fill_authors(author_list: list) -> Generator[list, str, str]:
  ''' Fills in the publications associated with multiple authors on a single paper / publication 
      from the main topic list. 
      Args
      ----
        author_list (list): a list of authors
  '''
  for authr in author_list: 
    encoded = quote(authr['name'])
    encoded_str = f'au:%22{encoded}%22'
    author_feed, author_count, author_upd = fill_feed(  
                          query=encoded_str, batch_step=0, batches=[100, 1000]
    )
    yield (author_feed, author_upd, encoded)
