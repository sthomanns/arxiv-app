from django.test import TestCase
from django.core.cache import cache
from .models import * 

class TestCases(TestCase):
  def setUp(self):
    self.lazyloader_topics = lazyload_topics()
    self.lazyloader_authors = lazyload_authors()

  def tearDown(self):
    pass

  def test_topics(self):
    print('testing if topics will be populated..')
    topics = self.lazyloader_topics()
    self.assertFalse(topics is None or len(topics) == 0, 'no topic data!')

  def test_authors(self):
    print('testing if authors will be populated..')
    authors, counts = self.lazyloader_authors()
    self.assertFalse(authors is None or len(authors) == 0, 'no authors data!')
    self.assertFalse(counts is None or len(counts) == 0, 'no counts data!')
  

##  TODO: additional test case ideas
##    - hit each HTTP endpoint
##    - verify incremental data gathering
##    - verify that updates aren't repeated within 24 hours