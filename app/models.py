from django.core.cache import cache
from operator import itemgetter
from typing import Generator, Union
from .feed import * 


def fetch_publication(author: str, publication_url: str): 
  ''' Gets a specific authors' publication, even if it is not included in the primary topic list,
      which is only for newest publications.
      Args
      ----
        author (str):  the author of the publication
        publication_url (str):  the URL of the publication on arXiv
  '''
  authors, counts = lazyload_authors()()
  # TODO replace with better identifier for author in cache
  for pub in authors[author]: 
    if (pub.id == publication_url):
      return pub
  return None


def lazyload_topics(fun=fill_topics):
  ''' Lazily loads new publications across the desired topics. 
      Args
      ----
        fun (function):  function passed in as a pre-requisite, which gets all the
                          publications across the four topics
  '''
  def loader():

    cache_topic = cache.get('cache_topic')

    if (cache_topic is not None and len(cache_topic) > 0): 
      return cache_topic

    cache_topic = list()
    for t, i in fun(None, None, None, None):                
      cache_topic += t 

    cache.set('cache_topic', cache_topic)
    return cache_topic

  return loader

def lazyload_authors(fun=fill_authors): 
  ''' Lazily loads collective works of each author to learn the most prolific ones, 
      using Redis to cache the final result as a sorted list.
      Args
      ----
        fun (function):  function passed in as a pre-requisite, which gets all the
                          publications across a list of authors
  '''
  def loader() -> Union[list, list]:

    cache_lens = cache.get('cache_lens')
    cache_publications = cache.get('cache_publications')

    if (cache_lens is not None and cache_publications is not None):   # TODO fix edge case & add test case for if the cache is only partially populated
      return ( cache_publications, cache_lens )                       #      maybe improve caching w/ Redis so that authors and publications have a unique key
    
    cache_lens = dict()
    cache_publications = dict()
    topics = lazyload_topics()()

    for topic in topics:
      if 'authors' in topic:
        for pub_list, ai, au in fun(topic['authors']):      # every paper listed on the topic page may contain 1+ authors    
          num_works = len(pub_list)                         # count the number of papers written by author
          cache_lens[au] = num_works
          cache_publications[au] = pub_list
          print(f'caching author data in redis server.. {au}')

    cache_lens = sorted(cache_lens.items(), key = itemgetter(1), reverse = True)
    cache.set('cache_lens', cache_lens)
    cache.set('cache_publications', cache_publications)

    return ( cache_publications, cache_lens )

  return loader